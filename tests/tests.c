#include <criterion/criterion.h>
#include "../vmachine/inc/vending_machine.h"


Test(keyboard, keyboard_scan){
    vending_machine_t ctx;
    keyboard_t kbd;
    /* link api before init keyboard */
    vm_link_api(&ctx);
    keyboard_init(&ctx, &kbd);
    /* waitng for input. cursor position is 0, after scan init */
    cr_assert_eq(keyboard_scan(&ctx, 1), action_busy, "Expected action busy result");
    cr_assert_eq(ctx.keyboard->cursor_position, 0, "Expected cursor position 0, found: %d", ctx.keyboard->cursor_position);

    /* Test disabled numeric keyboard */
    ctx.keyboard->numeric_keybord_enabled = false;

        /* user pressed numeric 1 */
    kbd.keyboard_pressed_callback(KEY_1);
    cr_assert_eq(ctx.keyboard->expected_len, 1, "Expected input len 1, found: %d", ctx.keyboard->expected_len); 
    cr_assert_eq(keyboard_scan(&ctx, 1), action_busy, "Expected action busy result");
    cr_assert_eq(ctx.keyboard->cursor_position, 0, "Expected cursor position 0, found: %d", ctx.keyboard->cursor_position);
    cr_assert_eq(ctx.keyboard->cancel, false, "Not expected KEY_CANCEL");
    cr_assert_eq(ctx.keyboard->enter, false, "Not expected ENTER");

     /* Test enabled numeric keyboard */   
    ctx.keyboard->numeric_keybord_enabled = true;
    kbd.keyboard_pressed_callback(KEY_1);
    cr_assert_eq(keyboard_scan(&ctx, 1), action_busy, "Expected action busy result");   
    cr_assert_eq(ctx.keyboard->cursor_position, 1, "Expected cursor position 1, found %d", ctx.keyboard->cursor_position);
    cr_assert_eq(ctx.keyboard->cancel, false, "Expected KEY_1, found KEY_CANCEL");
    cr_assert_eq(ctx.keyboard->enter, false, "Expected KEY_1, found ENTER");
    cr_assert_eq(ctx.keyboard->scan_buffer[0], KEY_1, "Expected KEY_1, found %c", ctx.keyboard->scan_buffer[0]);

    /* cursor position should remain 1, because we scan 1 char. value should remain KEY_1 */
    kbd.keyboard_pressed_callback(KEY_2);    
    cr_assert_eq(keyboard_scan(&ctx, 1), action_busy, "Expected action busy result");   
    cr_assert_eq(ctx.keyboard->cursor_position, 1, "Expected cursor position 1, found %d", ctx.keyboard->cursor_position);
    cr_assert_eq(ctx.keyboard->cancel, false, "Expected KEY_1, found KEY_CANCEL");
    cr_assert_eq(ctx.keyboard->enter, false, "Expected KEY_1, found ENTER");
    cr_assert_eq(ctx.keyboard->scan_buffer[0], KEY_1, "Expected KEY_1, found %c", ctx.keyboard->scan_buffer[0]);

    /* press enter, enter should be set, return value action enter */
    kbd.keyboard_pressed_callback(KEY_ENTER);
    cr_assert_eq(ctx.keyboard->enter, true, "Expected ENTER");
    cr_assert_eq(keyboard_scan(&ctx, 1), action_enter, "Expected action enter result");

    kbd.keyboard_pressed_callback(KEY_CANCEL);
    cr_assert_eq(ctx.keyboard->cancel, true, "Expected Cancel");
    cr_assert_eq(keyboard_scan(&ctx, 1), action_cancel, "Expected action cancel result");   

    /* waitng for input. cursor position is 0, after scan init */
    cr_assert_eq(keyboard_scan(&ctx, 4), action_busy, "Expected action busy result");    
    kbd.keyboard_pressed_callback(KEY_1);
    kbd.keyboard_pressed_callback(KEY_9);
    kbd.keyboard_pressed_callback(KEY_2);
    kbd.keyboard_pressed_callback(KEY_8);
    cr_assert_eq(ctx.keyboard->cursor_position, 4, "Expected cursor position 4, found %d", ctx.keyboard->cursor_position);
    cr_assert_eq(ctx.keyboard->scan_buffer[0], KEY_1, "Expected KEY_1, found %c", ctx.keyboard->scan_buffer[0]);
    cr_assert_eq(ctx.keyboard->scan_buffer[1], KEY_9, "Expected KEY_9, found %c", ctx.keyboard->scan_buffer[1]);
    cr_assert_eq(ctx.keyboard->scan_buffer[2], KEY_2, "Expected KEY_2, found %c", ctx.keyboard->scan_buffer[2]);
    cr_assert_eq(ctx.keyboard->scan_buffer[3], KEY_8, "Expected KEY_8, found %c", ctx.keyboard->scan_buffer[3]);
    /* buffer should remain unchanged*/
    kbd.keyboard_pressed_callback(KEY_5);
    cr_assert_eq(ctx.keyboard->scan_buffer[3], KEY_8, "Expected KEY_8, found %c", ctx.keyboard->scan_buffer[3]);

     /* press enter, enter should be set, return value action enter */
    kbd.keyboard_pressed_callback(KEY_ENTER);
    cr_assert_eq(ctx.keyboard->enter, true, "Expected ENTER");
    cr_assert_eq(keyboard_scan(&ctx, 1), action_enter, "Expected action enter result");

    kbd.keyboard_pressed_callback(KEY_CANCEL);
    cr_assert_eq(ctx.keyboard->cancel, true, "Expected Cancel");
    cr_assert_eq(keyboard_scan(&ctx, 1), action_cancel, "Expected action cancel result");     
}

/**
 * @brief Test linking api with context
 * 
 */
Test(vending_machine, link_api){
    vending_machine_t ctx;
    vm_link_api(&ctx);
    /* test api linking function */
    cr_assert_eq(ctx.api.deploy.depoloy_article, deploy_article, "Init deploy_article fail");
    cr_assert_eq(ctx.api.deploy.is_article_deployed, is_article_deployed, "Init is_article_deployed fail");   
}


Test(vending_machine, check_article_id){
    vending_machine_t ctx;
    keyboard_t kbd;
    article_stack_t test_stack = {
        .articles = {
            {
                .name = "Coca-cola",
                .price = 99.9,
                .id = 1,
                .position = { .row = 1, .column = 1},
                .type = CAN, 
            },
        }
    };
    /* link api before init keyboard */
    vm_link_api(&ctx);
    keyboard_init(&ctx, &kbd);
    ctx.article_stack = &test_stack;
    ctx.article_stack->articles[0].id = 1;

    /* enter single char */
    keyboard_scan(&ctx, 2);
    kbd.keyboard_pressed_callback(KEY_1);
    kbd.keyboard_pressed_callback(KEY_ENTER);
    keyboard_scan(&ctx, 2);
    cr_assert_eq(check_article_id_action(&ctx), action_error, "Expected that article ID 1 is invalid (01 is valid!)");

    /* try with id 01*/
    keyboard_scan(&ctx, 2);
    ctx.keyboard->numeric_keybord_enabled = true;  
    kbd.keyboard_pressed_callback(KEY_0);
    kbd.keyboard_pressed_callback(KEY_1);
    kbd.keyboard_pressed_callback(KEY_ENTER);
    keyboard_scan(&ctx, 2);
    cr_assert_eq(check_article_id_action(&ctx), action_success, "Expected that article ID 01 is valid");
}
