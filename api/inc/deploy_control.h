#include "../../stdlibs.h"

/** API za kontroler mehanike za izdavanje artikala */

/* izbacuje artikal iz odredjene pozicije u kasetu za preuzimanje */
void deploy_article(uint8_t row, uint8_t column);
/* ocitavanjem senzora proverava da li u kaseti ima proizvoda*/
bool is_article_deployed(void);
