#ifndef KEYBOARD_H_INC
#define KEYBOARD_H_INC

#include "../../stdlibs.h"

typedef enum{
    KEY_0 = '0',
    KEY_1,
    KEY_2,
    KEY_3,
    KEY_4,
    KEY_5,
    KEY_6,
    KEY_7,
    KEY_8,
    KEY_9,
    KEY_ENTER = 13,
    KEY_CANCEL = 24
}KEY_t;

/* KEY_t je enumeracija 0-9, ukljucujuci tastere ENTER i CANCEL*/
typedef void(*on_key_pressed_cb_t)(KEY_t key);

typedef struct{
    on_key_pressed_cb_t pressed;
}keyboard_emulator_t;

void keypad_set_on_key_pressed_(on_key_pressed_cb_t key_press_callback);

void keyboard_emulator_start();
void keyboard_emulator_end();

#endif //KEYBOARD_H_INC