#include "../../stdlibs.h"

/** API za rad sa display-om */
/* prikazuje tekst u 2 reda po 20 karaktera na display-u. Novi red se prikazuje nakon
karaktera '\n' */
void display_text(char *text_to_display);
