#include "../inc/api_list.h"

/**====================================================================================
API za rad sa uredjajem za placanje.
Uredjaj za placanje automatski prepoznaje novcanice i ostvaruje komunikaciju sa payment
servisima.
====================================================================================**/
/* vraca koliko je trenutno novca korisnik uneo u (dinarima, centima ...), bez obzira
na tip novca (papirni ili metalni) */
uint16_t payment_get_inserted_money(void){
    return 50;
}
/* vraca true ukoliko je kreditna kartica ubacena */
bool payment_is_credit_card(void){
    printf("Card inserted!\n");
    return true;
}
/* ukoliko je PIN ispravan, vrsi placanje za prosledjeni iznos i vraca true, u
suprotnom je false. Funkcija blokira proces/thread dok se ne zavrsi komunikacija sa
payment servisom */
bool payment_credit_card_pay(uint16_t money){
    printf("Payment success\n"); 
    return true;   
}
/* vraca kusur u vrednosti change. Ova funkcija je blokirajuca dok se ne zavrsi
izbacivanje kusura u posudu za prijem kusura */
void payment_return_change(uint16_t change){
        printf("Return change %d\n", change); 
}