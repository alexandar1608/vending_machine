#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

void deploy_article(uint8_t row, uint8_t column){
    printf("Deploying article in row %d, column %d\n", row, column);
}
/* ocitavanjem senzora proverava da li u kaseti ima proizvoda*/
bool is_article_deployed(void){
    printf("Article is deployed!\n");
    return true;
}
