#include "../inc/api_list.h"
#include <unistd.h>  //Header file for sleep(). man 3 sleep for details. 
#include <pthread.h> 
#include <stdio.h>
#include <string.h>

on_key_pressed_cb_t callback_key_pressed;
pthread_t thread_id;

#define IS_NUMERIC(x) (x >= '0') && (x<='9')

/**
 * @brief Thread to simulate physical keyboard
 * 
 * @param varg 
 * @return void* 
 */
void *keyboardThread(void *varg) 
{ 
    while(1){ 
        /* read input */
        char input = getchar();

        if(input == 'C'){ //simulate cancel button
            callback_key_pressed(KEY_CANCEL); 
        }
        else if(input == 'E') //simulate enter button
        {
            callback_key_pressed(KEY_ENTER);
        }
        else if(IS_NUMERIC(input))//translate input number to keys...
        {
            callback_key_pressed((KEY_t)(input));
        } 
    }
    return NULL; 
} 
/**
 * @brief Start keyboard emulator
 * 
 */
void keyboard_emulator_start()
{
    printf("Starting keyboard emulator\n");
    pthread_create(&thread_id, NULL, keyboardThread, NULL); 
}

/**
 * @brief Join threads..
 * 
 */
void keyboard_emulator_end(){
    pthread_join(thread_id, NULL);
}

/* prosleduje pointer na funkciju koja treba biti pozvana na key press event, kada se
prosledjuje parametar o pritisnutom tasteru */
void keypad_set_on_key_pressed_(on_key_pressed_cb_t key_press_callback){
   callback_key_pressed = key_press_callback; 
}
