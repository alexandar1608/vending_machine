set(TARGET api)

file(GLOB SOURCES "src/*.c")
include_directories(inc)

add_library(${TARGET} ${SOURCES})
target_link_libraries(${TARGET} pthread)