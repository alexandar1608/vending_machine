#include "stdlibs.h"
#include "vmachine/inc/vending_machine.h"



int main(){
    printf("****************************\n");
    vending_machine_t vmctx;
    vm_link_api(&vmctx);
    keyboard_emulator_start(); /*simulate hardware keyobard, so we can activate callback*/
    vm_init(&vmctx);
    vm_insert_test_articles(&vmctx);
    printf("\n****************************\n");
    /* let's rock */
    vm_main_worker(&vmctx);

    return 0;
}