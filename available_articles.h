#include "vmachine/inc/article.h"

    // char                name[ARTICLE_NAME_MAX_LEN];
    // float               price;
    // uint8_t             id;
    // article_position_t  position;
    // article_type_t      type;
    // bool                available;

article_stack_t articles_examples = {
    .articles = {
        {
            .name = "Coca-cola",
            .price = 99.9,
            .id = 1,
            .position = { .row = 1, .column = 1},
            .type = CAN, 
        },
        {
            .name = "Pepsi",
            .price = 89.9,
            .id = 2,
            .position = { .row = 1, .column = 2},
            .type = CAN, 
        }, 
        {
            .name = "Fanta",
            .price = 98.9,
            .id = 3,
            .position = { .row = 1, .column = 3},
            .type = CAN, 
        },

        {
            .name = "Coca-cola",
            .price = 99.9,
            .id = 7,
            .position = { .row = 2, .column = 1},
            .type = CAN, 
        },
        {
            .name = "Pepsi",
            .price = 89.9,
            .id = 8,
            .position = { .row = 2, .column = 2},
            .type = CAN, 
        }, 
        {
            .name = "Fanta",
            .price = 98.9,
            .id = 9,
            .position = { .row = 2, .column = 3},
            .type = CAN, 
        },  
    }      
};
