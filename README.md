# Vending mašina

**Zadatak**

Napisati kratak program u C-u koji emulira rad Vending mašine.
1. Pretpostaviti da mašina može izdavati 4 tipa artikala: 
   1. grickalice u kesici
   2. čokoladice 
   3. napitke u flašicama
   4. napitke u konzervama

2. Od svakog tipa se može naći po 6 različitih artikala.
3. Svaki proizvod je opisan nazivom, cenom i dvocifrenim identifikacionim brojem (01 - 99)
4. Poziciju jednog artikla definiše red i kolona
5. Na jednom redu se mogu naći samo artikli istog tipa (samo čokoladice ili samo napici ...)
6. Na Vending mašini je instaliran hardware koji omogućava rad sa papirnim i kovanim novcem i kreditnim karticama, kao i kontroler koji upravlja mehanikom za ubacivanje kupljenog artikla sa police u kasetu za preuzimanje.
7. Korisnik započinje kupovinu unosom ID-a artikla kojeg želi da kupi, nakon toga, korisnik vrši plaćanje artikla ubacivanjem novca ili kartice. U slučaju kartice, korisnik unosi 4-cifreni PIN kod preko tastature.
8. Ako korisnik ne unese dovoljnu količinu novca za izabrani artikal, nakon 50 sekundi, kupovina se poništava i novac se vraća korisniku
9. Ukoliko je plaćanje uspešno, mašina izbacuje artikal u kasetu za preuzimanje, čime se završava sesija kupovine.


Na mašini se nalazi embedded PC koji kontroliše sve funkcionalnosti i ostale uređaje (čitač kartica, brojač novca, mehaniku za izbacivanje proizvoda...).

Proizvođac je obezbedio C biblioteku za rad sa svim uređajima kroz sledeće C module i API funkcije koji se nalaze u folderu api

---

**Algoritam**

![algoritam](doc/algoritam.bmp)

---

**Class dijagram**

Verzija 1. Za sada ovako, biće izmena, verovatno.

![class](doc/classdiagram.jpg)

**State machine dijagram**

---

**TODO**
- [x] Kod zadatka hostovati na BitBucket kao private repositorijum i poslati link ka repozitorijumu uz dozvolu prava pristupa za aleksandar.atanasovski@htecgroup.com i sasa.takov@htecgroup.com
- [x] dopisati api funkcije koje emuliraju prisustvo tastature, brojača novca, mehaniku za izbacivanje novca, kako bi testiranje u konzoli bilo moguće
- [ ] README - proveriti u class dijagramu da li sam nesto propustio, srediti opis..
- [x] dodati [Criterion](https://criterion.readthedocs.io) lib za unit test

---