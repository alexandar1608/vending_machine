
#define MESSAGE_LEN 100

typedef struct{
    char text[MESSAGE_LEN];
}message_t;

/**
 * @brief
 * 
 */
typedef struct{
    message_t on_init;      /* display message on init state */
    message_t on_cancel;    /* display message on cancel operation */
    message_t on_error;     /* display message on error */
    message_t on_success;   /* display message on success */
}state_messages_t;

typedef struct{
    state_messages_t read_article_id;       /* Messages for read_article_id state */
    state_messages_t check_article_id;      /* Messages for check article id state */
    state_messages_t select_payment;        /* Messages for select payment state */
    state_messages_t check_payment_method;  /* Messages for check payment state */
    state_messages_t insert_cash;           /* Messages for cash payment state */
    state_messages_t inserd_card;           /* Messages for insert card state */
    state_messages_t enter_pin;             /* Messages for enter pin state */
    state_messages_t card_payment;          /* Messages for card payment state */
    state_messages_t deploy_article;        /* Messages for deploy article state */
    state_messages_t check_deployment;      /* Messages for checking if article is deployed */
    state_messages_t return_money;          /* Messages for return money state */
}messages_t;

/**
 * @brief All messages
 * 
 */
messages_t messages;
