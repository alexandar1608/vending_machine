/**
 * @file keyboard_scaner.h
 * @author aleksandar
 * @brief Scan keyboard 
 * @version 0.1
 * @date 2019-10-03
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#include "../../api/inc/api_list.h"

/**
 * @brief Keyboard class..
 * 
 */
typedef struct{
    bool    numeric_keybord_enabled;
    char    scan_buffer[4];
    uint8_t cursor_position;
    uint8_t expected_len;
    uint8_t input_len;
    bool    enter;
    bool    cancel;
    void(*keyboard_pressed_callback)(KEY_t key);
}keyboard_t;

extern keyboard_t keyboard;






