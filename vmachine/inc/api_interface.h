#include "../../stdlibs.h"
#include "../../api/inc/api_list.h"

/**
 * @brief payment api interface
 * 
 */
typedef struct{
    uint16_t    (*get_inserted_money)(void); /* return iserted money value*/
    bool        (*is_credit_card)(void); /* check if credit card is inserted*/
    bool        (*credit_card_pay)(uint16_t); /* return true if payment was successfuly */
    void        (*return_change)(uint16_t); /* return change to the user*/
}payment_api_t;

/**
 * @brief deploy control api interface
 * 
 */
typedef struct{
    void (*depoloy_article)(uint8_t row, uint8_t column); /* deploy article */
    bool (*is_article_deployed) (); /* check if article is deployed */
}deploy_control_api_t;

/**
 * @brief display api interface
 * 
 */
typedef struct{
    void (*display_text)(char * text_to_display); /* function to display text */
}display_api_t;

/**
 * @brief keybord api interface
 * 
 */
typedef struct{
    void(*set_on_key_pressed)(on_key_pressed_cb_t); /* fucntion for setting on key pressed callback*/
}keyboard_api_t;

/**
 * @brief api interfaces class
 * 
 */
typedef struct{
    payment_api_t           payment;
    deploy_control_api_t    deploy;
    display_api_t           display;
    keyboard_api_t          keyboard;
}api_t;


