#ifndef ARTICLE_H_INC
#define ARTICLE_H_INC
#include <stdint.h>
#include <stdbool.h>

#define ARTICLE_NAME_MAX_LEN 50
#define ARTICLE_STACK_SIZE 6 * 4
/**
 * @brief Article - Snack, chocolate, drink - bottle or can 
 * 
 */
typedef enum{
    SNACKS,
    CHOCOLATE,
    BOTTLE,
    CAN
}article_type_t;

/**
 * @brief Article position. Definied with row and column
 * 
 */
typedef struct __attribute__((__packed__)){
    uint8_t row;
    uint8_t column;
}article_position_t;

/**
 * @brief Article class. Name is inserted for prinitng..
 * 
 */
typedef struct __attribute__((__packed__)){
    char                name[ARTICLE_NAME_MAX_LEN];
    float               price;
    uint8_t             id; /* > 1 and  < 99*/
    article_position_t  position;
    article_type_t      type;
}article_t;

typedef struct __attribute__((__packed__)){
    article_t articles[ARTICLE_STACK_SIZE];
}article_stack_t;


#endif //ARTICLE_H_INC