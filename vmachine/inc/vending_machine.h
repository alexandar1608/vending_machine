#include "../../api/inc/api_list.h"
#include "messages.h"
#include "api_interface.h"
#include "article.h"
#include "keyboard.h"

typedef struct vending_machine_t vending_machine_t;
typedef struct vm_state_t vm_state_t;

void keyboard_init(vending_machine_t* ctx, keyboard_t* kbrd);

/**
 * @brief VM action function output result type
 * 
 */
typedef enum{
    action_busy,
    action_success,
    action_error,
    action_cancel,
    action_enter
}action_res_t;

/**
 * @brief keyboard_scan_res_t
 * 
 */
typedef enum{
    keyboard_busy,
    keyboard_enter,
    keyboard_cancel
}keyboard_scan_res_t;

/**
 * @brief Vending machine state type
 * 
 */
struct vm_state_t{
    action_res_t(*action)(vending_machine_t *); /* execute during state */
    struct vm_state_t* on_action_success;       /* state action result with success */
    struct vm_state_t* on_action_fail;          /* state action result with fail */
    struct vm_state_t* on_action_enter;         /* pressed enter during action */
    struct vm_state_t* on_action_cancel;        /* pressed cancel during action */
    state_messages_t*  messages;                      /* messages to display */
};

/**
 * @brief Vending machine context type
 * 
 */
struct vending_machine_t{
    api_t               api;
    struct vm_state_t*  state;              /* current VM state */
    article_stack_t*    article_stack;      /* list of all available articles */
    article_t*          article;            /* current, working article*/
    keyboard_t*         keyboard;           /* keyboard pointer */
    float               inserted_money;     /* inserted money */
    float               change;             /* change */
};

/**
 * @brief link provided api with context
 * 
 * @param ctx vending machine context
 */
void vm_link_api(vending_machine_t* ctx);

/**
 * @brief Init state machine
 * 
 * @param ctx vending machine context
 */
void vm_init(vending_machine_t* ctx);

/**
 * @brief Vending machine main worker..
 * 
 * @param ctx vending machine context
 */
void vm_main_worker(vending_machine_t* ctx);

/**
 * @brief Insert articles into vending machine.. won't work without candys
 * 
 * @param ctx vending machine context
 */
void vm_insert_test_articles(vending_machine_t* ctx);

/**
 * @brief Keyboard scaner
 * 
 * @param ctx 
 * @param expected_len 
 * @return action_res_t 
 */
keyboard_scan_res_t keyboard_scan(vending_machine_t* ctx, uint8_t expected_len);

/**
 * @brief Check if article id is valid and available
 * 
 * @param ctx vending machine context
 * @return action_res_t 
 */
action_res_t check_article_id_action(vending_machine_t* ctx);