#include "vending_machine.h"
#include "../../available_articles.h"
#include <string.h>
#include <time.h>

#define PAYMENT_TIMEOUT 50

/**
 * @brief Keyboard scaner
 * 
 * @param ctx 
 * @param expected_len 
 * @return action_res_t 
 */
keyboard_scan_res_t keyboard_scan(vending_machine_t* ctx, uint8_t expected_len){
    static enum{
        init_scan,
        handle_scan
    }scan_state = init_scan;

    switch (scan_state)
    {
    case init_scan:
        memset(ctx->keyboard->scan_buffer, 0, 4); //TODO - change this
        ctx->keyboard->numeric_keybord_enabled = true;
        ctx->keyboard->expected_len = expected_len;
        ctx->keyboard->input_len = 0;
        ctx->keyboard->cursor_position = 0;
        scan_state = handle_scan;
        //no need for brake .. dont waste time
    case handle_scan:
        if(ctx->keyboard->enter)
        {
            ctx->keyboard->enter = false;
            ctx->keyboard->numeric_keybord_enabled = false;
            scan_state = init_scan;
            return keyboard_enter;
        }
        if(ctx->keyboard->cancel)
        {
            ctx->keyboard->cancel = false;
            ctx->keyboard->numeric_keybord_enabled = false;
            scan_state = init_scan;
            return keyboard_cancel;
        }
        break;
    }
    return keyboard_busy;
}

/******** STATES *******/
vm_state_t read_article_id_state;
vm_state_t check_article_id_state;
vm_state_t select_payment_state;
vm_state_t check_payment_method_state;
vm_state_t insert_cash_state;
vm_state_t insert_card_state;
vm_state_t enter_card_pin_state;
vm_state_t card_payment_state;
vm_state_t deploy_article_state;
vm_state_t check_deployment_state;
vm_state_t return_money_state;
/******** ACTONS *******/

/**
 * @brief Action method for read article id
 * 
 * @param ctx vending machine context
 * @return action_res_t 
 */
action_res_t read_article_id_action(vending_machine_t* ctx){
    switch(keyboard_scan(ctx, 2))
    {
        case keyboard_enter:
            return action_enter;
        break;
        case keyboard_cancel:
            return action_cancel;
        break;
        case keyboard_busy:
            return action_busy;
        break;
    }
}

/**
 * @brief Check if article id is valid and available
 * 
 * @param ctx vending machine context
 * @return action_res_t 
 */
action_res_t check_article_id_action(vending_machine_t* ctx){
    if(ctx->keyboard->input_len == 2){ /* article id is 01 - 99, two digits */
        article_t* art = ctx->article_stack->articles;
        int id;
    //searching for available article.. 
        sscanf(ctx->keyboard->scan_buffer, "%d", &id);
        printf("Selected article ID: %d\n", id);
        for(int i = 0; i < sizeof(article_stack_t) / sizeof(article_t); i++){
            if(id != art->id)
            {
                art++;
                continue;
            } 
            //found desired article
            ctx->article = art;
            char msg[100];
            snprintf(msg, sizeof(msg), "Selected: %s - %.2f din\n", ctx->article->name, ctx->article->price);
            ctx->api.display.display_text(msg);
            return action_success;
        }
        //missing id
        return action_error;
    }
    else
    {
        return action_error;
    }
}

/**
 * @brief Select payment method (cash or card)
 * 
 * @param ctx vending machine context
 * @return action_res_t 
 */
action_res_t select_payment_action(vending_machine_t* ctx){
    switch(keyboard_scan(ctx, 1))
    {
        case keyboard_enter:
            return action_enter;
        break;
        case keyboard_cancel:
            return action_cancel;
        break;
        case keyboard_busy:
            return action_busy;
        break;
    }
}

/**
 * @brief Check if proper option is selected
 * 
 * @param ctx vending machine context
 * @return action_res_t 
 */
action_res_t check_payment_method_action(vending_machine_t* ctx){
    if(ctx->keyboard->input_len == 1){ /* 0 - Cash; 1 - Card */
        switch(ctx->keyboard->scan_buffer[0])
        {
            case '0':
                ctx->state->on_action_success = &insert_cash_state;
                return action_success;
            break;
            case '1':
                ctx->state->on_action_success = &insert_card_state;
                return action_success;
            break;
            default:
                return action_error;
            break;
        }
    }
    else
    {
        return action_error;
    }    
}

/**
 * @brief Whate for money
 * 
 * @param ctx vending machine context
 * @return action_res_t 
 */
action_res_t wait_for_money_action(vending_machine_t* ctx){
    static time_t start, now;
    static enum{
        init,
        wait
    }wfma_state = init;

    switch (wfma_state)
    {
    case init:
        time (&start);   
        wfma_state = wait;
        break;
    
    case wait:
        ctx->inserted_money = ctx->api.payment.get_inserted_money();
        if( ctx->inserted_money >= ctx->article->price)
        {
            ctx->change = ctx->inserted_money - ctx->article->price;
            return action_success;
        }
        time (&now);
        double dif = difftime(now, start);
        if (dif >= PAYMENT_TIMEOUT)
        {
            wfma_state = init;
            return action_cancel;
        }
        break;
    }
    return action_busy;
}

/**
 * @brief Deploy article
 * 
 * @param ctx vending machine context
 * @return action_res_t 
 */
action_res_t deploy_article_action(vending_machine_t* ctx){
    ctx->api.deploy.depoloy_article(ctx->article->position.row, ctx->article->position.column);
    return action_success;
}

/**
 * @brief Return change
 * 
 * @param ctx vending machine context
 * @return action_res_t 
 */
action_res_t return_money_action(vending_machine_t* ctx){
    int change_int = ctx->change * 100;
    if(change_int > 0) 
    {
        printf("Oh rly %f\n", ctx->change);
        ctx->api.payment.return_change(ctx->change);
    }
    return action_success;
}

/**
 * @brief Check if article is depolyed
 * 
 * @param ctx 
 * @return action_res_t 
 */
action_res_t check_deployment_action(vending_machine_t* ctx){
    if (ctx->api.deploy.is_article_deployed()){
        ctx->article->id = 0; //item is no longer available       
        return action_success;
    } 
    return action_error;
}

/**
 * @brief Wait for card
 * 
 * @param ctx 
 * @return action_res_t 
 */
action_res_t wait_for_card_action(vending_machine_t* ctx){
    static time_t start, now;
    static enum{
        init,
        wait
    }card_state = init;

    switch (card_state)
    {
        case init:
            time (&start);   
            card_state = wait;
            break;
        
        case wait:
            if(ctx->api.payment.is_credit_card())
            {
                card_state = init;
                return action_success;
            }
            else
            {
                time (&now);
                double dif = difftime(now, start);
                if (dif >= PAYMENT_TIMEOUT)
                {
                    card_state = init;
                    return action_cancel;
                }
            }      
            break;
    }
    return action_busy;
}

action_res_t enter_card_pin_action(vending_machine_t* ctx){
    switch(keyboard_scan(ctx, 4))
    {
        case keyboard_enter:
            return action_enter;
        break;
        case keyboard_cancel:
            return action_cancel;
        break;
        case keyboard_busy:
            return action_busy;
        break;
    }
}

action_res_t card_payment_action(vending_machine_t* ctx){
    if(ctx->api.payment.credit_card_pay(ctx->article->price))
    {
        return action_success;
    }
    else
    {
        return action_error;
    }
}


/* */

/**
 * @brief read_article_id_state
 * 
 */
vm_state_t read_article_id_state = {
    .messages           = &messages.read_article_id,
    .action             = read_article_id_action,
    .on_action_success  = &check_article_id_state,
    .on_action_fail     = &read_article_id_state,
    .on_action_cancel   = &read_article_id_state,
    .on_action_enter    = &check_article_id_state
};

/**
 * @brief check_article_id_state
 * 
 */
vm_state_t check_article_id_state = {
    .messages           = &messages.check_article_id,
    .action             = check_article_id_action,
    .on_action_success  = &select_payment_state,
    .on_action_fail     = &read_article_id_state,
};

/**
 * @brief select_payment_state
 * 
 */
vm_state_t select_payment_state = {
    .messages           = &messages.select_payment,
    .action             = select_payment_action,
    .on_action_success  = &check_payment_method_state,
    .on_action_fail     = &read_article_id_state,
    .on_action_cancel   = &read_article_id_state,
    .on_action_enter    = &check_payment_method_state
};

/**
 * @brief check_payment_method_state
 * 
 */
vm_state_t check_payment_method_state = {
    .messages           = &messages.check_payment_method,
    .action             = check_payment_method_action,
    .on_action_success  = &check_article_id_state,
    .on_action_fail     = &read_article_id_state,
    .on_action_cancel   = &read_article_id_state,
    .on_action_enter    = &check_article_id_state
};

/**
 * @brief cash_payment_state
 * 
 */
vm_state_t insert_cash_state = {
    .messages           = &messages.insert_cash,
    .action             = wait_for_money_action,
    .on_action_success  = &deploy_article_state,
    .on_action_fail     = &read_article_id_state,
    .on_action_cancel   = &return_money_state,
    .on_action_enter    = &insert_cash_state
};

/**
 * @brief insert_card_state
 * 
 */
vm_state_t insert_card_state = {
    .messages           = &messages.inserd_card,
    .action             = wait_for_card_action,
    .on_action_success  = &enter_card_pin_state,
    .on_action_fail     = &read_article_id_state,
    .on_action_cancel   = &read_article_id_state,
    .on_action_enter    = &insert_card_state    
};

/**
 * @brief enter_card_pin_state
 * 
 */
vm_state_t enter_card_pin_state = {
    .messages           = &messages.enter_pin,
    .action             = enter_card_pin_action,
    .on_action_success  = &card_payment_state,
    .on_action_fail     = &read_article_id_state,
    .on_action_cancel   = &read_article_id_state,
    .on_action_enter    = &card_payment_state    
};

/**
 * @brief card_payment_state
 * 
 */
vm_state_t card_payment_state = {
    .messages           = &messages.card_payment,
    .action             = card_payment_action,
    .on_action_success  = &deploy_article_state,
    .on_action_fail     = &read_article_id_state,
    .on_action_cancel   = &read_article_id_state,
    .on_action_enter    = &card_payment_state    
};

/**
 * @brief deploy_article_state
 * 
 */
vm_state_t deploy_article_state = {
    .messages           = &messages.deploy_article,
    .action             = deploy_article_action,
    .on_action_success  = &check_deployment_state,
    .on_action_fail     = &return_money_state,
    .on_action_cancel   = &return_money_state,
    .on_action_enter    = &deploy_article_state    
};

/**
 * @brief deploy_article_state
 * 
 */
vm_state_t check_deployment_state = {
    .messages           = &messages.deploy_article,
    .action             = check_deployment_action,
    .on_action_success  = &return_money_state,
    .on_action_fail     = &return_money_state,
    .on_action_cancel   = &return_money_state,
    .on_action_enter    = &check_deployment_state    
};

/**
 * @brief deploy_article_state
 * 
 */
vm_state_t return_money_state = {
    .messages           = &messages.return_money,
    .action             = return_money_action,
    .on_action_success  = &read_article_id_state,
    .on_action_fail     = &return_money_state,
    .on_action_cancel   = &return_money_state,
    .on_action_enter    = &return_money_state    
};


/***********************/

/**
 * @brief link provided api with context
 * 
 * @param ctx vending machine context
 */
void vm_link_api(vending_machine_t* ctx){
    /* init deploy api */
    ctx->api.deploy.depoloy_article = deploy_article;
    ctx->api.deploy.is_article_deployed = is_article_deployed;
    /* init display api */
    ctx->api.display.display_text = display_text;
    /* init keyboard api */
    ctx->api.keyboard.set_on_key_pressed = keypad_set_on_key_pressed_;
    /* init payment api */
    ctx->api.payment.credit_card_pay = payment_credit_card_pay;
    ctx->api.payment.get_inserted_money = payment_get_inserted_money;
    ctx->api.payment.is_credit_card = payment_is_credit_card;
    ctx->api.payment.return_change = payment_return_change;
}

/**
 * @brief Insert articles into vending machine.. won't work without candys
 * 
 * @param ctx vending machine context
 */
void vm_insert_test_articles(vending_machine_t* ctx){
    ctx->article_stack = &articles_examples;
}

extern keyboard_t keyboard; //mora ovako.. callback key pressed je bez pointera za context vm, pa moramo nekako da linkujemo tastaturu i objekat
extern void keyobard_init(vending_machine_t* ctx);
/**
 * @brief Init state machine
 * 
 * @param ctx vending machine context
 */
void vm_init(vending_machine_t* ctx){
    /* Set starting point*/
    ctx->state = &read_article_id_state;
    /* Set keyboard */
    keyboard_init(ctx, &keyboard);
}

/**
 * @brief Vending machine main worker
 * 
 * @param ctx vending machine context
 */
void vm_main_worker(vending_machine_t* ctx){
    static enum{
        on_init_state,
        on_action,
    }worker_state = on_init_state;
    while(true){
        switch(worker_state){
            case on_init_state:
                ctx->api.display.display_text(ctx->state->messages->on_init.text);
                /* start state action */
                worker_state = on_action;
            break;
            case on_action:
                switch(ctx->state->action(ctx))
                {
                    case action_success:
                    {
                        /* display message */
                        ctx->api.display.display_text(ctx->state->messages->on_success.text);
                        /* set new state*/
                        ctx->state = ctx->state->on_action_success;
                        /* return worker to init */
                        worker_state = on_init_state;
                    }
                    break;
                    case action_error:
                    {
                        /* display message */
                        ctx->api.display.display_text(ctx->state->messages->on_error.text); 
                        /* set new state */
                        ctx->state = ctx->state->on_action_fail;
                        /* return worker to init */
                        worker_state = on_init_state; 
                    }
                    break;
                    case action_cancel:
                    {
                        /* display message */
                        ctx->api.display.display_text(ctx->state->messages->on_cancel.text); 
                        /* set new state */
                        ctx->state = ctx->state->on_action_cancel; 
                        /* return worker to init */
                        worker_state = on_init_state;
                    }
                    break;
                    case action_enter:
                    {
                        /* set new state */
                        ctx->state = ctx->state->on_action_enter; 
                        /* return worker to init */
                        worker_state = on_init_state;
                    }
                    break;
                    default:
                        break;
                }
            break;
        }
    }
}

