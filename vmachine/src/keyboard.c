#include "vending_machine.h"
#include <string.h> 

keyboard_t keyboard;
keyboard_t* keyboard_ptr; //for mocking
/**
 * @brief Callback for keyboard
 * 
 * @param key 
 */
void keyboard_pressed_callback(KEY_t key){
    keyboard_ptr->enter = false;
    keyboard_ptr->cancel = false;
    switch(key){
        case KEY_ENTER:
            keyboard_ptr->enter = true;
            keyboard_ptr->cursor_position = 0;
        break;
        case KEY_CANCEL:
            keyboard_ptr->cancel = true;
            keyboard_ptr->cursor_position = 0;
        break;
        default:
            if(keyboard_ptr->numeric_keybord_enabled){
                if(keyboard_ptr->expected_len > keyboard_ptr->cursor_position)
                {   
                    keyboard_ptr->scan_buffer[keyboard_ptr->cursor_position++] = key;
                    keyboard_ptr->input_len++;
                }
            }
        break;
    }   
}

/**
 * @brief Keyboard initialization
 * 
 * @param ctx Vending machine context
 */
void keyboard_init(vending_machine_t* ctx, keyboard_t* kbrd){
    keyboard_ptr = kbrd;
    memset((char *)keyboard_ptr, 0, sizeof(keyboard_t)); //*reset all flegs and buffers
    keyboard_ptr->keyboard_pressed_callback = keyboard_pressed_callback;
    ctx->api.keyboard.set_on_key_pressed(keyboard_pressed_callback);
    ctx->keyboard = keyboard_ptr;
}

