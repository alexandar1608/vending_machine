#include "messages.h"

messages_t messages = {
    /**
     * @brief Messages for read_article_id state
     * 
     */
    .read_article_id = {
        .on_init = "Select article: ",
        .on_cancel = "Canceled\n",
        .on_error = "",
        .on_success = ""
    },
    /**
     * @brief Messages for check article id state
     * 
     */
    .check_article_id = {
        .on_init = "",
        .on_cancel = "Canceled\n",
        .on_error = "Invalid article id\n",
        .on_success = ""
    },
    /**
     * @brief Messages for select payment state
     * 
     */
    .select_payment = {
        .on_init = "Select payment (0 - Cash, 1 - Card):",
        .on_cancel = "Canceled\n",
        .on_error = "Invalid selection\n",
        .on_success = ""
    },
     /**
     * @brief Messages for checking selected payment state
     * 
     */   
    .check_payment_method = {
        .on_init = "",
        .on_cancel = "Canceled\n",
        .on_error = "Invalid selection\n",
        .on_success = ""
    },
    /**
     * @brief Messages for select payment state
     * 
     */
    .insert_cash = {
        .on_init = "Please insert money\n>",
        .on_cancel = "Canceled\n",
        .on_error = "Error\n",
        .on_success = ""
    },
    .inserd_card = {
        .on_init = "Please, insert card\n",
        .on_cancel = "Canceled\n",
        .on_error = "Error\n",
        .on_success = ""
    },
    .enter_pin = {
        .on_init = "Please, enter pin: ",
        .on_cancel = "Canceled\n",
        .on_error = "Error\n",
        .on_success = ""
    },
    .deploy_article = {
        .on_init = "",
        .on_cancel = "Canceled\n",
        .on_error = "Error\n",
        .on_success = ""
    },
    .check_deployment = {
        .on_init = "",
        .on_cancel = "",
        .on_error = "Error deploying article\n",
        .on_success = "Please pick up your article\n"
    },
    .return_money = {
        .on_init = "",
        .on_cancel = "",
        .on_error = "",
        .on_success = ""       
    }
};
